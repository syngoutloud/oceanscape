﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class moveRandomPath : MonoBehaviour
{
    Vector3 startingPoint;
    float changeDirectionInterval;
    float distanceFromSphereCenter;
    float sphereRadius;
    public float speed;
    private Vector3 direction;
    public GameObject soundSpace;
    public handleOceanGestures HandleOceanGestures;
    bool hasChangedDirection = false;

    // Start is called before the first frame update
    void Start()
    {

        startingPoint = transform.localPosition;
        changeDirectionInterval = Random.Range(1, 3);
        InvokeRepeating("setDirection", 0, changeDirectionInterval);
        sphereRadius = 0.01f;
        

    }

    void setDirection()
    {

        Vector3 existingDirection = direction;
        
        //direction = (new Vector3(Random.Range(-1.0f, 1.0f), 0.0f, Random.Range(-1.0f, 1.0f))).normalized;
        

        direction = Vector3.Lerp(direction, (new Vector3(Random.Range(-1.0f, 1.0f), 0.0f, Random.Range(-1.0f, 1.0f))).normalized, (speed) * Time.deltaTime);

        Quaternion rotateDir = Quaternion.LookRotation(direction);
        transform.DORotateQuaternion(rotateDir, 0.5f);


        hasChangedDirection = false;


    }

    void goInOppositeDirection()
    {
        direction = new Vector3(-direction.x * Random.Range(0.9f,1.1f), 0, -direction.z * Random.Range(0.9f, 1.1f));
        
        Quaternion rotateDir = Quaternion.LookRotation(direction);
        transform.DORotateQuaternion(rotateDir, 0.5f);
        

    }



    // Update is called once per frame
    void Update()
    {
        if (HandleOceanGestures.hasExitedDrawer == true)
        {
            distanceFromSphereCenter = Vector3.Distance(transform.position, soundSpace.transform.position);

           // print(distanceFromSphereCenter);

            transform.localPosition += direction * speed * Time.deltaTime;

            if (distanceFromSphereCenter > 2.5f && hasChangedDirection == false)
            {
                goInOppositeDirection();
                hasChangedDirection = true;
            }

            

        }
       
    }
}
