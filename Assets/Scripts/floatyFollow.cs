﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TouchScript.Gestures;

public class floatyFollow : MonoBehaviour
{

    public GameObject targetObject;
    bool isPressed = false;
    public TapGesture doubleTap;

    // Start is called before the first frame update
    void Start()
    {
        doubleTap = gameObject.GetComponent<TapGesture>();
        doubleTap.Tapped += DoubleTap_Tapped;
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;
    }

    // Update is called once per frame
    void Update()
    {
        //if (isPressed == false)
        //{
        //    transform.DOMove(new Vector3(targetObject.transform.position.x + 0.5f, 1, targetObject.transform.position.z + 0.5f), 3);

        //}
    }

    private void DoubleTap_Tapped(object sender, System.EventArgs e)
    {
        Debug.Log("double tappy");

        //Invoke("focusThisSource", 0.1f);

    }

    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {

            print("RELESS!");
            isPressed = false;
           
        }

    }

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            isPressed = true;
            print("PESS!");
            //DOTween.Kill(gameObject);
            //transform.DOLocalMove(new Vector3(0, 0, 0), 0.3f).SetEase(Ease.OutBack);
        }

    }
}
