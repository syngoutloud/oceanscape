﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;

public class handleSourceGestures : MonoBehaviour
{

    public TapGesture doubleTap;
    public bool isFocused = false;
    Vector3 startingScale;
    handleState HandleState;
    public GameObject colorSphere;
    Vector3 colorSphereInitialScale;
    Vector3 colorSphereFocusedScale;
    public GameObject spaceFloor;
    Vector3 floorInitialScale;
    public GameObject iconIso;
    public GameObject iconTopDown;
    public GameObject coverParent;
    public GameObject audioSource;
    Vector3 currentSourcePos;
    //public GameObject currentCoverCircleCropped;
    //public GameObject currentCoverSquare;
    float distanceFromCenter;
    bool isMuted = false;
    public GameObject oceanSphere;
    Vector3 oceanSphereStartingScale;
    public bool hasExitedSphere = true;
    public AudioSource spotifyAudioSource;
    GameObject[] oceanChildren;
    public GameObject oceanParticles;
    public GameObject drawerTarget;
    bool isPressed = false;
    public GameObject iconSpotify;


    // Start is called before the first frame update
    void Start()
    {
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
        startingScale = transform.localScale;
        colorSphereFocusedScale = new Vector3(0.38f, 0.31f, 0.38f);
        colorSphereInitialScale = colorSphere.transform.localScale;
        doubleTap = gameObject.GetComponent<TapGesture>();
        doubleTap.Tapped += DoubleTap_Tapped;
        floorInitialScale = spaceFloor.transform.localScale;
        oceanSphereStartingScale = oceanSphere.transform.localScale;
        hasExitedSphere = true;
        oceanChildren = GameObject.FindGameObjectsWithTag("oceanChild");
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;
    }

    private void DoubleTap_Tapped(object sender, System.EventArgs e)
    {
        Debug.Log("double tappy");

        if (hasExitedSphere == false)
        {
            Invoke("focusThisSource", 0.1f);
        }
        
        

    }

    // Update is called once per frame
    void Update()
    {
        distanceFromCenter = Vector3.Distance(transform.localPosition, new Vector3(0, 0, 0));
        //print("dist = " + distanceFromCenter);


        if (HandleState.spotifyIsFocused == false)
        {
            if (distanceFromCenter > 4f && hasExitedSphere == false)
            {
              //  print("outtttS");
                
                spotifyAudioSource.DOFade(0, 1);
                spotifyAudioSource.DOPitch(0, 1);
                GetComponent<spinGeneric>().speed = 0;
                transform.DOLocalRotate(new Vector3(0, 0, 0), 0.5f);


                hasExitedSphere = true;

                iconSpotify.GetComponent<SpriteRenderer>().DOFade(1, 0.2f);
            }

            if (distanceFromCenter < 4f && hasExitedSphere == true)
            {

               // print("innnnS");
                spotifyAudioSource.DOFade(1, 1);
                spotifyAudioSource.DOPitch(1, 1);
                GetComponent<spinGeneric>().speed = -2;

                hasExitedSphere = false;


                if (HandleState.spotifyIsPaused)
                {
                    HandleState.togglePlayPauseSpotify();
                }

                iconSpotify.GetComponent<SpriteRenderer>().DOFade(0, 0.2f);
                
            }

            
        }

    }

    public void focusThisSource()
    {
        //grab current pos, we want to keep the audio there
        currentSourcePos = transform.position;

        
        GetComponent<SphereCollider>().enabled = false;
        GetComponent<spinGeneric>().enabled = false;
        if (HandleState.viewIsIso == true)
        {
            transform.DORotate(new Vector3(-50, 0, 0), 0.3f);

        }
        else
        {
            transform.DORotate(new Vector3(0, 0, 0), 0.3f);

        }
        transform.DOMove(new Vector3(0, 0, 0), 0.5f).SetEase(Ease.OutBack);
        audioSource.transform.DOMove(currentSourcePos, 0.5f).SetEase(Ease.OutBack);
        transform.DOScale(startingScale * 10, 0.5f).SetEase(Ease.OutBack);
        colorSphere.transform.DOScale(colorSphereFocusedScale, 0.5f);
        HandleState.hideDrawer();

        if (HandleState.spotifyIsPaused && HandleState.spotifyIconStatusPaused.activeInHierarchy == true)
        {
            HandleState.spotifyIconStatusPaused.SetActive(false);
        }

        HandleState.Invoke("showPlaylist", 0.3f);
        GameObject.Find("sphereBackgroundColor").GetComponent<SpriteRenderer>().DOFade(0, 0.3f);
        spaceFloor.transform.DOScale(new Vector3(0, 0, 0), 0.4f);
        iconIso.SetActive(false);
        iconTopDown.SetActive(false);
        coverParent.transform.DOLocalMove(new Vector3(0, 0.1f, 0.0069f), 0.3f);
        HandleState.currentAlbumArtSquare.GetComponent<SpriteRenderer>().DOFade(1, 0.2f);
        HandleState.currentAlbumArtCircle.GetComponent<SpriteRenderer>().DOFade(0, 0.2f);
        oceanSphere.transform.DOScale(new Vector3(0, 0, 0), 0.2f);

        foreach (GameObject child in oceanChildren)
        {
            child.GetComponent<moveRandomPath>().enabled = false;

        }

        oceanParticles.SetActive(false);


        HandleState.spotifyIsFocused = true;
    }

    public void defocusThisSource()
    {
        
        GetComponent<SphereCollider>().enabled = true;
        GetComponent<spinGeneric>().enabled = true;
        transform.DORotate(new Vector3(0, 0, 0), 0.5f);
        colorSphere.transform.DOScale(colorSphereInitialScale, 0.5f);
        transform.DOMove(currentSourcePos, 0.3f).SetEase(Ease.OutBack);
        transform.DOScale(startingScale, 0.3f).SetEase(Ease.InOutQuad);

        HandleState.hidePlaylist();
        GameObject.Find("sphereBackgroundColor").GetComponent<SpriteRenderer>().DOFade(1, 0.3f);
        //GameObject.Find("spaceFloor").SetActive(true);
        spaceFloor.transform.DOScale(floorInitialScale, 0.3f);
        iconIso.SetActive(true);
        iconTopDown.SetActive(true);
        coverParent.transform.DOLocalMove(new Vector3(0, 0, 0), 0.3f);
        HandleState.currentAlbumArtSquare.GetComponent<SpriteRenderer>().DOFade(0, 0.3f).SetDelay(0.2f);
        HandleState.currentAlbumArtCircle.GetComponent<SpriteRenderer>().DOFade(1, 0.3f).SetDelay(0.2f);
        HandleState.spotifyIsFocused = false;
        if (oceanSphere.transform.position.z < -4) //ie it's in the drawer
        {
            oceanSphere.transform.DOScale(oceanSphereStartingScale, 0.3f);
        }
        else
        {
            oceanSphere.transform.DOScale(oceanSphereStartingScale * 0.5f, 0.3f);
        }
        
        Invoke("enableChildrenWalking", 0.4f);
        oceanParticles.SetActive(true);

    }

    void enableChildrenWalking()
    {
        foreach (GameObject child in oceanChildren)
        {
            child.GetComponent<moveRandomPath>().enabled = true;

        }
    }


    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
           // print("spotify pressed");
            isPressed = true;
            
        }

    }



    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {

            //print("spotify released");

            if (hasExitedSphere == true)
            {
                transform.DOMove(new Vector3(drawerTarget.transform.position.x, transform.position.y, drawerTarget.transform.position.z), 0.3f).SetEase(Ease.OutBack);

                if (HandleState.spotifyIsPaused == true)
                {
                   // print("dooooo");
                    HandleState.handlePlayPauseSpotify();
                }

            }

            isPressed = false;

 

        }

    }


}
