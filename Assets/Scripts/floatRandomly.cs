﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class floatRandomly : MonoBehaviour
{

    public float initialYPos;

    public float speed = 0.01f;

    private Vector3 direction;

    float floorRadius = 3f;

    float sphereRadius = 3f;

    float changeDirectionInterval;

    handleState HandleState;

    //public bool turnDirectionEnabled = true;

    float distanceFromSphereCenter;

    Vector3 startingPoint;

    bool hasMovedToDrawer;

    float distanceFromParent;


    // Start is called before the first frame update
    void Start()
    {

        startingPoint = transform.localPosition;
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
        changeDirectionInterval = Random.Range(3, 5);
        initialYPos = transform.localPosition.y;
        InvokeRepeating("setDirection", 0, changeDirectionInterval);
    }

    void setDirection()
    {
        Quaternion oldDirection = transform.rotation;

        direction = (new Vector3(Random.Range(-1.0f, 1.0f), 0.0f, Random.Range(-1.0f, 1.0f))).normalized;

        Quaternion rotateDir = Quaternion.LookRotation(direction);

        transform.rotation = Quaternion.Lerp(oldDirection, rotateDir, Time.time * speed);

        //transform.DORotateQuaternion(rotateDir, changeDirectionInterval).SetEase(Ease.InOutSine);
            

        //print("called setDirection");

    }

    //void setPointToFloatTo

    //void goInOppositeDirection()
    //{
    //    direction = -direction;
    //    //if (turnDirectionEnabled == true)
    //    //{
    //        Quaternion rotateDir = Quaternion.LookRotation(direction);
    //        transform.DORotateQuaternion(rotateDir, 1f);
    //    //}

    //}

    void Update()
    {

        //distanceFromSphereCenter = (transform.position - GameObject.Find("soundSpace").transform.position).magnitude;

        //print("dist" + distanceFromSphereCenter);

        //if (distanceFromSphereCenter < sphereRadius) // if we're in the soundspace
        //{
        //transform.localPosition += direction * speed * Time.deltaTime;

        //    //crap clamp - change to distance from sphere center
        //    if (transform.localPosition.x > floorRadius || transform.localPosition.x < -floorRadius)
        //    {
        //        goInOppositeDirection();
        //    }

        //    if (transform.localPosition.z > floorRadius || transform.localPosition.z < -floorRadius)
        //    {
        //        goInOppositeDirection();
        //    }

        //}


        

        



    }

}
