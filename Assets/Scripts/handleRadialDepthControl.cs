﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript;
using TouchScript.Gestures;
using TouchScript.Gestures.TransformGestures;
using DG.Tweening;
using LiquidVolumeFX;
using Obi;

public class handleRadialDepthControl : MonoBehaviour
{

    bool isPressed = false;
    public GameObject visibleHandle;
    public float angle;
    public GameObject visibleHandleParent;
    public AudioSource sourceShore;
    public AudioSource sourceOpenSea;
    public AudioSource sourceUnderSea;
    float liquidLevelMin = 0.25f;
    float liquidLevelMax = 0.85f;
    public GameObject liquidSphere;
    public LiquidVolume liquid;

    float normalizedAngle;

    public ObiSolver obiSolver;
    //public ObiEmitter obiEmitter;

    void setObiGravity()
    {
        obiSolver.transform.eulerAngles = new Vector3(normalizedAngle * 180, 0, 0);
        //obiSolver.parameters.damping = normalizedAngle;
        //obiEmitter.enabled = true;
    }



    // Start is called before the first frame update
    void Start()
    {

       
        

        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;
        // liquid = liquidSphere.GetComponent<LiquidVolume>();


        

    }

    //void OnDisable()
    //{
    //    Debug.Log("PrintOnDisable: script was disabled");
    //}

    public void resetControls()
    {
        normalizedAngle = 0;
        visibleHandleParent.transform.eulerAngles = new Vector3(90, 0, 0);

       // transform.position = visibleHandle.transform.position;
    }

    


    // Update is called once per frame
    void Update()
    {
        if (isPressed == true)
        {
            rotate();
            setAudioAndLiquid();
            //obiEmitter.enabled = false;
            setObiGravity();
        }

        
    }

    float map(float s, float a1, float a2, float b1, float b2) //(value, low1, high1, low2, high2);
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

    public void setAudioAndLiquid()
    {
        sourceShore.volume = map(1-normalizedAngle, 0.5f, 1, 0, 1);
        sourceUnderSea.volume = map(1-normalizedAngle, 0, 0.5f, 1, 0);

        if (1-normalizedAngle > 0.5f)
        {
            sourceOpenSea.volume = map(1-normalizedAngle, 0.5f, 0.75f, 1, 0);
            
        }
        else
        {
            sourceOpenSea.volume = map(1-normalizedAngle, 0.25f, 0.5f, 0, 1);
        }

        //liquid.level = map(normalizedAngle, 0, 1, liquidLevelMin, liquidLevelMax);

        //obiSolver.enabled = false;
        
        //obiSolver.enabled = true;

    }





    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("released!");

            isPressed = false;


            // handlePaddleTouch();

            resetHiddenDraggablePos();

            

        }

    }

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("pressed!");

            isPressed = true;



        }

    }

    public void resetHiddenDraggablePos()
    {
        transform.position = visibleHandle.transform.position;
    }



    
    void rotate()
    {

        Vector3 vectorToTarget = this.transform.position - visibleHandleParent.transform.position;

        //Debug.DrawLine(hiddenDragHandle.transform.position, needle.transform.position, Color.green, Time.deltaTime, false);
        angle = (Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg) + 90;

        if (transform.localPosition.x < 0.1f)
        {
            visibleHandleParent.transform.eulerAngles = new Vector3(40, 0, angle);

        }

        //lol free jazz normalization

        if (angle < 0)
        {
            normalizedAngle = (-angle / 90)/2;
        }
        else
        {
            normalizedAngle = 1-((angle-180)/90 / 2);
        }

        //print(normalizedAngle);

    }

   
}
