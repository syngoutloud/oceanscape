﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;
using Obi;

public class handleOceanGestures : MonoBehaviour
{

    public TapGesture doubleTap;
    handleState HandleState;

    Vector3 startingScale;
    Vector3 startingPos;
    public GameObject colorSphere;
    Vector3 colorSphereInitialScale;
    Vector3 colorSphereFocusedScale;
    public GameObject spaceFloor;
    Vector3 floorInitialScale;
    public GameObject iconIso;
    public GameObject iconTopDown;
    public GameObject iconCloseX;
    public GameObject spotify;
    Vector3 spotifyStartingScale;
    public GameObject liquid;
    float distanceFromCenter;
    public GameObject audioSourceShallow;
    public GameObject audioSourceDeep;
    public GameObject audioSourceMid;
    public GameObject oceanParticles;
    public GameObject oceanParticlesContainer;
    Vector3 initialOceanScale;
    Vector3 initialOceanContainerScale;
    public ObiActor obiParticleEmitter;
    bool hasExitedSphere;
    bool isFocused = false;

    public AudioSource oceanSourceShore;
    public AudioSource oceanSourceOpenSea;
    public AudioSource oceanSourceUnderSea;

    public handleRadialDepthControl HandleRadialDepthControl;

    public GameObject[] oceanChildren;

    public bool hasExitedDrawer = false;

    bool isPressed = false;

    public AudioSource audioSeagulls;
    public AudioSource audioBoat;
    public AudioSource audioKids;
    

    // Start is called before the first frame update
    void Start()
    {
        spotifyStartingScale = spotify.transform.localScale;
        startingPos = transform.localPosition;
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
        startingScale = transform.localScale;
        colorSphereFocusedScale = new Vector3(0.41f, 0.41f, 0.41f);
        colorSphereInitialScale = colorSphere.transform.localScale;
        doubleTap = gameObject.GetComponent<TapGesture>();
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;
        initialOceanScale = oceanParticles.transform.localScale;
        initialOceanContainerScale = oceanParticlesContainer.transform.localScale;

        doubleTap.Tapped += DoubleTap_Tapped;
        floorInitialScale = spaceFloor.transform.localScale;

        oceanSourceOpenSea.volume = 0;
        oceanSourceUnderSea.volume = 0;
        oceanSourceShore.volume = 0;

        Invoke("startAudioBedsAfterDelay", 1);

        oceanChildren = GameObject.FindGameObjectsWithTag("oceanChild");

    }


    void startAudioBedsAfterDelay()
    {
        //print("yoink");
        oceanSourceOpenSea.gameObject.SetActive(true);
        oceanSourceUnderSea.gameObject.SetActive(true);
        oceanSourceShore.gameObject.SetActive(true);
    }

    private void DoubleTap_Tapped(object sender, System.EventArgs e)
    {
        Debug.Log("double tappy ocean");

        if (hasExitedDrawer == true)
        {
            if (HandleState.oceanIsFocused == false)
            {
                HandleState.oceanIsFocused = true;
                HandleState.showOceanControls();
            }
        }

       
        

        HandleState.lastTouchedOceanObject = gameObject;
        HandleState.setLastTouchedOceanObject();

    }

    // Update is called once per frame
    void Update()
    {
        distanceFromCenter = Vector3.Distance(transform.localPosition, new Vector3(0, 0, 0));
        //print("dist = " + distanceFromCenter);


        if (HandleState.oceanIsFocused == false)
        {
            if (distanceFromCenter > 4f && hasExitedSphere == false)
            {
                //print("outttt");
                //audioSource.GetComponent<AudioSource>().DOFade(0, 0.2f);
                //isMuted = true;
                //obiParticleEmitter
                //obiParticleEmitter.UpdateParticleProperties();
                oceanParticles.transform.DOScale(new Vector3(0, 0, 0), 0.8f).SetEase(Ease.OutSine).OnComplete(disableParticles);
                disableSphericalForce();

                //HandleRadialDepthControl.setAudioAndLiquid();

                //float currentVolOpenSea = HandleRadialDepthControl.sourceOpenSea.volume;
                //float currentVolShore = HandleRadialDepthControl.sourceShore.volume;
                //float currentVolUnderSea = HandleRadialDepthControl.sourceUnderSea.volume;

                oceanSourceOpenSea.DOFade(0, 2);
                oceanSourceShore.DOFade(0, 2);
                oceanSourceUnderSea.DOFade(0, 2);

                audioBoat.DOFade(0, 2);
                audioKids.DOFade(0, 2);
                audioSeagulls.DOFade(0, 2);

                HandleRadialDepthControl.resetControls();

                hasExitedSphere = true;
            }

            if (distanceFromCenter < 4f && hasExitedSphere == true)
            {

                //print("in");
                //audioSource.GetComponent<AudioSource>().DOFade(0.9f, 0.2f);
                //isMuted = false;
                //obiParticleEmitter.
                //obiParticleEmitter.transform.DOScale(new Vector3(1, 1, 1), 0);
                //oceanParticles.transform.parent = transform.parent;
                //oceanParticles.transform.DOLocalMove(new Vector3(0, 0, 0), 0.5f);
                oceanParticles.transform.DOScale(new Vector3(0.67f, 0.67f, 0.67f), 0);
                enableParticles();
                //Invoke("enableSphericalForce", 1);
                enableSphericalForce();

                //HandleRadialDepthControl.setAudioAndLiquid();

                float currentVolOpenSea = HandleRadialDepthControl.sourceOpenSea.volume;
                float currentVolShore = HandleRadialDepthControl.sourceShore.volume;
                float currentVolUnderSea = HandleRadialDepthControl.sourceUnderSea.volume;

                audioBoat.DOFade(1, 2);
                audioKids.DOFade(1, 2);
                audioSeagulls.DOFade(1, 2);

                // print(currentVolOpenSea);

                //oceanSourceOpenSea.DOFade(currentVolOpenSea * 1, 2);
                oceanSourceShore.DOFade(1, 2);
               // oceanSourceUnderSea.DOFade(currentVolUnderSea * 1, 2);

                hasExitedSphere = false;


                

            }


            
        }

        //if (hasExitedSphere == false)
        //{
            
       // }
        
       
        
    }

    void disableParticles()
    {
        obiParticleEmitter.enabled = false;
    }

    void enableParticles()
    {
        obiParticleEmitter.enabled = true;
    }

    void enableSphericalForce()
    {
        GetComponent<ObiSphericalForceZone>().enabled = true;
        //oceanParticles.transform.parent = GameObject.Find("sphereSpace").transform;
        oceanParticles.transform.DOLocalMove(new Vector3(0, 0, 0), 0.5f);

    }

    void disableSphericalForce()
    {

        GetComponent<ObiSphericalForceZone>().enabled = false;
        //oceanParticles.transform.parent = transform.parent;
        //oceanParticles.transform.DOLocalMove(new Vector3(0, 0, 0), 0.5f);

    }


    public void focusThisSource()
    {

        startingPos = transform.localPosition;

        if (HandleState.viewIsIso == false)
        {
            HandleState.viewIsIso = true;
            HandleState.setCamView();
        }


        spotify.transform.DOScale(new Vector3(0, 0, 0), 0.2f);

        HandleState.oceanIsFocused = true;
        GetComponent<SphereCollider>().enabled = false;

        if (HandleState.viewIsIso == true)
        {
            transform.DORotate(new Vector3(-50, 0, 0), 0.3f);
            transform.DOMove(new Vector3(0, 0, 4.8f), 0.6f).SetEase(Ease.OutBack);

        }
        else
        {
            transform.DORotate(new Vector3(0, 0, 0), 0.3f);
            transform.DOMove(new Vector3(0, -1, 0), 0.6f).SetEase(Ease.OutBack);


        }

        transform.DOScale(startingScale * 10, 0.6f).SetEase(Ease.OutBack);
        liquid.transform.DOScale(new Vector3(0.216225f, 0.216225f, 0.216225f), 0.8f).SetEase(Ease.OutBack);
        liquid.transform.DOMove(new Vector3(0, 0, -0.13f), 0.6f).SetEase(Ease.OutBack);
        colorSphere.transform.DOScale(colorSphereFocusedScale, 0.6f);

        GameObject.Find("sphereBackgroundColor").GetComponent<SpriteRenderer>().DOFade(0, 0.3f);
        spaceFloor.transform.DOScale(new Vector3(0, 0, 0), 0.6f);
        iconIso.SetActive(false);
        iconTopDown.SetActive(false);
        iconCloseX.SetActive(true);
        HandleState.Invoke("showOceanControls", 0.6f);
        HandleRadialDepthControl.Invoke("resetHiddenDraggablePos", 1);
        //oceanParticles.transform.DOScale(initialOceanScale * 0.75f, 0.4f);
        //oceanParticlesContainer.transform.DOScale(initialOceanContainerScale * 0.8f, 0.4f);


    }

    public void defocusThisSource()
    {

        //if (HandleState.viewIsIso == true)
        //{
        //    HandleState.viewIsIso = false;
        //    HandleState.setCamView();
        //}


        
        spotify.transform.DOScale(spotifyStartingScale, 0.3f).SetEase(Ease.OutBack).SetDelay(0.3f);
        GetComponent<SphereCollider>().enabled = true;
        transform.DOMove(startingPos, 0.3f).SetEase(Ease.OutBack);
        transform.DORotate(new Vector3(0, 0, 0), 0.5f);
        colorSphere.transform.DOScale(colorSphereInitialScale, 0.3f);
        transform.DOScale(startingScale * 0.5f, 0.3f).SetEase(Ease.InOutQuad);
        liquid.transform.DOScale(new Vector3(1, 1, 1), 0.3f);
        GameObject.Find("sphereBackgroundColor").GetComponent<SpriteRenderer>().DOFade(1, 0.3f);
        spaceFloor.transform.DOScale(floorInitialScale, 0.3f);
        //iconIso.SetActive(true);
        //iconTopDown.SetActive(true);
        iconCloseX.SetActive(false);
        HandleState.hideOceanControls();
        liquid.transform.DOLocalMove(new Vector3(0, 0, 0), 0.3f);
        //oceanParticles.transform.DOScale(initialOceanScale, 0.4f);
        //oceanParticlesContainer.transform.DOScale(initialOceanContainerScale, 0.4f);

        //transform.DORotate(new Vector3(-50, 0, 0), 0.3f);
        //transform.DOMove(new Vector3(0, 0, 4.8f), 0.6f).SetEase(Ease.OutBack);
        //HandleState.viewIsIso = false;

        Invoke("resetFocusIsFalse", 1);

        HandleState.viewIsIso = false;
        HandleState.setCamView();

    }

    void resetFocusIsFalse()
    {
        HandleState.oceanIsFocused = false;
    }

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("oceanMasterPressed");

            //foreach (GameObject child in oceanChildren)
            //{
            //    child.transform.parent = GameObject.Find("ocean").transform;
            //}

            //if (hasExitedDrawer == true)
            //{
            //    foreach (GameObject child in oceanChildren)
            //    {
            //        child.transform.parent = GameObject.Find("ocean").transform;
            //    }
                    
            //}
        }

    }



    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {

            //print("ocean parent released");

            //foreach (GameObject child in oceanChildren)
            //{
            //    child.transform.parent = GameObject.Find("soundSpace").transform;
            //}

                isPressed = false;

            if (hasExitedSphere == false)
            {
                //transform.DOLocalMove(new Vector3(0, 0, 0), 0.3f).SetEase(Ease.OutBack);
                transform.DOScale(startingScale * 0.5f, 0.5f).SetEase(Ease.OutBack);

                if (hasExitedDrawer == false)
                {
                    foreach (GameObject child in oceanChildren)
                    {
                        
                        
                        child.transform.DOScale(2, 0.5f).SetEase(Ease.OutBack);
                        //child.transform.DOLocalMove(new Vector3(Random.Range(-0.5f, 0.5f), 0, Random.Range(-0.5f, 0.5f)), 0.6f).SetEase(Ease.OutBack);

                        switch (child.name)
                        {
                            case "Seagulls":
                                child.transform.DOLocalMove(new Vector3(0, 0, 0.5f), 0.5f).SetEase(Ease.OutBack);
                                break;

                            case "Jetski":
                                child.transform.DOLocalMove(new Vector3(-0.5f, 0, 0), 0.5f).SetEase(Ease.OutBack);
                                break;
                            case "Kids Playing":
                                child.transform.DOLocalMove(new Vector3(0.5f, 0, 0), 0.5f).SetEase(Ease.OutBack);
                                break;
                            default:
                                break;
                        }

                        child.GetComponent<SphereCollider>().enabled = true;
                    }
                }

               

                hasExitedDrawer = true;

            }
            else
            {
                transform.DOLocalMove(startingPos,0.3f).SetEase(Ease.OutBack);
                transform.DOScale(startingScale, 0.3f).SetEase(Ease.OutBack);

                foreach (GameObject child in oceanChildren)
                {
                    child.transform.DOScale(0.25f, 0.3f).SetEase(Ease.OutBack);

                    switch (child.name)
                    {
                        case "Seagulls":
                            child.transform.DOLocalMove(new Vector3(0, 0, 0.05f), 0.3f).SetEase(Ease.OutBack);
                            break;

                        case "Jetski":
                            child.transform.DOLocalMove(new Vector3(-0.05f, 0, 0f), 0.3f).SetEase(Ease.OutBack);
                            break;
                        case "Kids Playing":
                            child.transform.DOLocalMove(new Vector3(0.05f, 0, 0f), 0.3f).SetEase(Ease.OutBack);
                            break;
                        default:
                            break;
                    }


                    child.GetComponent<SphereCollider>().enabled = false;
                }

                hasExitedDrawer = false;
            }
            
        }

    }

}
