﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class walkRandomly : MonoBehaviour
{
    public float initialYPos;

    public float speed = 0.4f;

    private Vector3 direction;

    float floorRadius = 1.9f;

    float sphereRadius = 3.5f;

    float changeDirectionInterval;

    handleState HandleState;

    public bool turnDirectionEnabled = true;

    float distanceFromSphereCenter;

    Vector3 startingPoint;

    bool hasMovedToDrawer;



    void Start()
    {
        startingPoint = transform.localPosition;
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
        changeDirectionInterval = Random.Range(7, 10);
        initialYPos = transform.localPosition.y;
        InvokeRepeating("setDirection", 0, changeDirectionInterval);
        
    }

    void setDirection()
    {
        if (distanceFromSphereCenter < sphereRadius)
        {
            direction = (new Vector3(Random.Range(-1.0f, 1.0f), 0.0f, Random.Range(-1.0f, 1.0f))).normalized;

            if (turnDirectionEnabled == true)
            {
                Quaternion rotateDir = Quaternion.LookRotation(direction);
                transform.DORotateQuaternion(rotateDir, 1f);
            }
        }

      
    }

    void goInOppositeDirection()
    {
        direction = -direction;
        if (turnDirectionEnabled == true)
        {
            Quaternion rotateDir = Quaternion.LookRotation(direction);
            transform.DORotateQuaternion(rotateDir, 1f);
        }

    }

    void Update()
    {

        distanceFromSphereCenter = (transform.position - GameObject.Find("soundSpace").transform.position).magnitude;

        if (distanceFromSphereCenter < sphereRadius)
        {
            transform.localPosition += direction * speed * Time.deltaTime;

            //crap clamp - change to distance from sphere center
            if (transform.localPosition.x > floorRadius || transform.localPosition.x < -floorRadius)
            {
                goInOppositeDirection();
            }

            if (transform.localPosition.z > floorRadius || transform.localPosition.z < -floorRadius)
            {
                goInOppositeDirection();
            }

            if (GetComponent<Animator>() != null)
            {
                GetComponent<Animator>().enabled = true;
            }


        }
        else
        {
            if (GetComponent<Animator>() != null)
            {
                GetComponent<Animator>().enabled = false;
            }
        }

        if (HandleState.soundScapeIsInDrawer == true)
        {
            if (hasMovedToDrawer == false)
            {
                transform.DOLocalMove(startingPoint, 0.3f);
                hasMovedToDrawer = true;
            }


        }
        else
        {
            hasMovedToDrawer = false;
        }

        

    }

    

    









}
