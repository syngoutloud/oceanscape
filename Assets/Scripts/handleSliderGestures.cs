﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;
using LiquidVolumeFX;

public class handleSliderGestures : MonoBehaviour
{

    public GameObject sliderHandleVisible;
    public GameObject sliderRail;
    bool isPressed = false;
    float sliderRailMinPos;
    float sliderRailMaxPos;
    float sliderNormalizedValue;
    public AudioSource sourceShore;
    public AudioSource sourceOpenSea;
    public AudioSource sourceUnderSea;
    float liquidLevelMin = 0.25f;
    float liquidLevelMax = 0.85f;
    public GameObject liquidSphere;
    public LiquidVolume liquid;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;

        GetComponent<PressGesture>().StateChanged += pressHandler;

        sliderRailMaxPos = 1.9f;

        sliderRailMinPos = -2.9f;

        //print("max = " + sliderRailMaxPos);

        //print("min = " + sliderRailMinPos);

        liquid = liquidSphere.GetComponent<LiquidVolume>();

    }

    // Update is called once per frame
    void Update(){

        if (isPressed == true)
        {
            sliderHandleVisible.transform.localPosition = new Vector3(sliderHandleVisible.transform.localPosition.x, Mathf.Clamp(transform.localPosition.y,sliderRailMinPos,sliderRailMaxPos), sliderHandleVisible.transform.localPosition.z);
            sliderNormalizedValue = 1 - Mathf.Clamp(((transform.localPosition.y - sliderRailMinPos) / (sliderRailMaxPos - sliderRailMinPos)),0,1);
            print("norm = " + sliderNormalizedValue);

            sourceShore.volume = map(sliderNormalizedValue, 0.5f, 1, 0, 1);
            sourceUnderSea.volume = map(sliderNormalizedValue, 0, 0.5f, 1, 0);

            if (sliderNormalizedValue > 0.5f) {
                sourceOpenSea.volume = map(sliderNormalizedValue, 0.5f, 0.75f, 1, 0);
            } else {
                sourceOpenSea.volume = map(sliderNormalizedValue, 0.25f, 0.5f, 0, 1);
            }

            liquid.level = map(1-sliderNormalizedValue, 0, 1, liquidLevelMin, liquidLevelMax);
        }
    }

    float map(float s, float a1, float a2, float b1, float b2) //(value, low1, high1, low2, high2);
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            print("slider released!");
            isPressed = false;
            transform.localPosition = sliderHandleVisible.transform.localPosition;
        }

    }

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            print("slider pressed!");
            isPressed = true;

        }

    }

}
