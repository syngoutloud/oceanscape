﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TouchScript.Gestures.TransformGestures;
using TMPro;
using Shapes2D;

public class handleState : MonoBehaviour
{

    public bool soundScapeIsInDrawer = false;
    public GameObject targetSpace;
    public GameObject targetDrawer;
    public GameObject jungleSpace;
    public GameObject closeX;
    public SphereCollider jungleSpaceSphereCollider;
    public TransformGesture jungleSpaceTransformGesture;
    public AudioSource jungleSpaceBackgroundAudio;
    public AudioSource raincloudAudio;
    public GameObject sphereBackgroundColor;
    public Color skyColorDay;
    public Color skyColorNight;
    public Color skyColorOff;
    public Color cameraBkgColorOff;
    public Color cameraBkgColorDay;
    public GameObject drawerBackgroundCircle;
    public GameObject rainCloud;
    //public GameObject rainParticles;
    Vector3 rainCloudStartingPos;
    public GameObject timeControl;
    //public AudioSource audioLoopCrickets;
    //public AudioSource audioLoopBirds;
    //public AudioSource audioLoopBatFlapping;
    public GameObject skyOverlay;
    public GameObject timeControlRotator;
    public GameObject timeControlHiddenDragHandle;
    public GameObject timeControlVisibleDragHandle;
    public Light mainLight;
    public GameObject mainLightParent;
    public bool viewIsIso = false;
    public GameObject camParent;
    public GameObject iconIso;
    public GameObject iconTopDown;
    public GameObject iconIsoHighlight;
    public GameObject iconTopDownHighlight;
    //public GameObject iconBack;
    public GameObject playlist;
    
    public GameObject oceanControls;
    public AudioSource currentTrack;
    public AudioClip clipAutobahn;
    public AudioClip clipModel;
    public AudioClip clipRadio;
    public GameObject selectionHighlight;
    public GameObject currentAlbumArtSquare;
    public GameObject currentAlbumArtCircle;
    public GameObject albumArtBlueSquare;
    public GameObject albumArtRedSquare;
    public GameObject albumArtYellowSquare;
    public GameObject albumArtBlueCircle;
    public GameObject albumArtRedCircle;
    public GameObject albumArtYellowCircle;
    public GameObject colorSphere;
    public Material sphereBlue;
    public Material sphereRed;
    public Material sphereYellow;

    public bool oceanIsFocused = false;
    public bool spotifyIsFocused = false;

    public int currentTrackNum;

    public GameObject oceanFullscreenBkg;
    public GameObject sourceSpotify;
    public GameObject sourceOcean;

    public GameObject drawerOcean;
    public GameObject drawerSpotify;

    public TextMeshPro trackTitle;
    public TextMeshPro trackArtist;

    public GameObject lastTouchedOceanObject;
    //public string lastTouchedOceanObject;
    public TextMeshPro selectedObjectTitle;
    public TextMeshPro labelMotionStyle;
    public TextMeshPro labelPlaybackStyle;

    public GameObject selectedObjectRing;

    public Sprite iconSpotifyPlay;
    public Sprite iconSpotifyPause;

    public bool spotifyIsPaused = false;

    //0 auto, 1 model, 2 radio

    public GameObject spotifyButtonPlayPause;

    public GameObject spotifyIconStatusPaused;




    // Start is called before the first frame update
    void Start()
    {
        viewIsIso = false;
        setCamView();
        //Time.timeScale = 0.1f;
        currentTrackNum = 0;
        Application.targetFrameRate = 60;
        soundScapeIsInDrawer = false;
        //rainCloudStartingPos = rainCloud.transform.localPosition;
        //setJungleDraggableState();
    }

    public void setLastTouchedOceanObject()
    {

            hideOceanLabels();

            //print("called setLastTouchedOceanObject: " + lastTouchedOceanObject);
            selectedObjectTitle.text = lastTouchedOceanObject.name;

            selectedObjectRing.transform.position = lastTouchedOceanObject.transform.position;

            switch (lastTouchedOceanObject.name)
            {
            
                case "Seagulls":
                    labelMotionStyle.text = "Random Path";
                    labelPlaybackStyle.text = "Play on Collide";
                    selectedObjectRing.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                selectedObjectRing.GetComponent<Shape>().settings.innerCutout = new Vector2(0.92f, 0.92f);
                break;
                case "Jetski":
                    labelMotionStyle.text = "Random Path";
                    labelPlaybackStyle.text = "Play Randomly";
                    selectedObjectRing.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                selectedObjectRing.GetComponent<Shape>().settings.innerCutout = new Vector2(0.92f, 0.92f);
                break;
                case "Kids Playing":
                    labelMotionStyle.text = "Set Path";
                    labelPlaybackStyle.text = "Infinite Loop";
                    selectedObjectRing.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                selectedObjectRing.GetComponent<Shape>().settings.innerCutout = new Vector2(0.92f, 0.92f);
                break;
                case "Ocean":
                    labelMotionStyle.text = "None";
                    labelPlaybackStyle.text = "Infinite Loop";
                    selectedObjectRing.transform.localScale = new Vector3(0.85f, 0.85f, 0.85f);
                    selectedObjectRing.GetComponent<Shape>().settings.innerCutout = new Vector2(0.88f, 0.88f);
                break;
                default:
                    break;
        
        }

        Invoke("showOceanLabels", 0.1f);

    }

    void hideOceanLabels()
    {
        selectedObjectTitle.DOFade(0, 0);
        labelMotionStyle.DOFade(0, 0);
        labelPlaybackStyle.DOFade(0, 0);
    }

    void showOceanLabels()
    {
        selectedObjectTitle.DOFade(1, 0.2f);
        labelPlaybackStyle.DOFade(1, 0.2f).SetDelay(0.1f);
        labelMotionStyle.DOFade(1, 0.2f).SetDelay(0.2f);
    }

    public void togglePlayPauseSpotify()
    {
        if (spotifyIsPaused == false)
        {
            spotifyIsPaused = true;
        }
        else
        {
            spotifyIsPaused = false;
        }

        handlePlayPauseSpotify();
    }

    public void handlePlayPauseSpotify()
    {

        //print("called handlePlayPauseSpotify");

        if (spotifyIsPaused == true)
        {

            currentTrack.DOPitch(0, 0.3f);
            spotifyButtonPlayPause.GetComponent<SpriteRenderer>().sprite = iconSpotifyPlay;

            if (sourceSpotify.GetComponent<handleSourceGestures>().hasExitedSphere == true)
            {
                //print("yeahhhh");
                spotifyIconStatusPaused.SetActive(false);
                switch (currentTrackNum)
                {
                    case 0:
                        albumArtBlueCircle.GetComponent<SpriteRenderer>().DOColor(Color.white, 0.5f);
                        break;
                    case 1:
                        albumArtRedCircle.GetComponent<SpriteRenderer>().DOColor(Color.white, 0.5f);
                        break;
                    case 2:
                        albumArtYellowCircle.GetComponent<SpriteRenderer>().DOColor(Color.white, 0.5f);
                        break;
                    default:
                        break;
                }
            }


        }
        else
        {
            currentTrack.DOPitch(1, 0.3f);
            spotifyButtonPlayPause.GetComponent<SpriteRenderer>().sprite = iconSpotifyPause;

            if (spotifyIsFocused == false)
            {
                sourceSpotify.GetComponent<spinGeneric>().enabled = true;

            }

            
        }
    }


    

    // Update is called once per frame
    void Update()
    {
        if (oceanIsFocused == true)
        {
            selectedObjectRing.transform.position = lastTouchedOceanObject.transform.position;
        }
    }

    public void playSelectedTrack()
    {
        currentTrack.Stop();

        switch (currentTrackNum)
        {
            case 0:
                currentTrack.clip = clipAutobahn;
                selectionHighlight.transform.DOLocalMoveY(-3.45f, 0.1f);

                albumArtBlueSquare.SetActive(true);
                albumArtRedSquare.SetActive(false);
                albumArtYellowSquare.SetActive(false);

                albumArtBlueCircle.SetActive(true);
                albumArtRedCircle.SetActive(false);
                albumArtYellowCircle.SetActive(false);

                currentAlbumArtCircle = albumArtBlueCircle;
                currentAlbumArtSquare = albumArtBlueSquare;

                colorSphere.GetComponent<MeshRenderer>().material = sphereBlue;

                //trackArtist.text = "Tycho";
                trackTitle.text = "Tycho – Coastal Brake";

                break;
            case 1:
                currentTrack.clip = clipModel;
                selectionHighlight.transform.DOLocalMoveY(-4.45f, 0.1f);
                
                albumArtBlueSquare.SetActive(false);
                albumArtRedSquare.SetActive(true);
                albumArtYellowSquare.SetActive(false);

                albumArtBlueCircle.SetActive(false);
                albumArtRedCircle.SetActive(true);
                albumArtYellowCircle.SetActive(false);

                currentAlbumArtCircle = albumArtRedCircle;
                currentAlbumArtSquare = albumArtRedSquare;

                colorSphere.GetComponent<MeshRenderer>().material = sphereRed;

                //trackArtist.text = "The Lively Ones";
                trackTitle.text = "The Lively Ones – Surf Rider";

                break;
            case 2:
                currentTrack.clip = clipRadio;
                selectionHighlight.transform.DOLocalMoveY(-5.45f, 0.1f);
                
                albumArtBlueSquare.SetActive(false);
                albumArtRedSquare.SetActive(false);
                albumArtYellowSquare.SetActive(true);

                albumArtBlueCircle.SetActive(false);
                albumArtRedCircle.SetActive(false);
                albumArtYellowCircle.SetActive(true);

                currentAlbumArtCircle = albumArtYellowCircle;
                currentAlbumArtSquare = albumArtYellowSquare;

                colorSphere.GetComponent<MeshRenderer>().material = sphereYellow;

                //trackArtist.text = "Bobby Darin";
                trackTitle.text = "Bobby Darin – Beyond The Sea";


                break;
            default:
                break;
        }

        currentTrack.Play();
    }

    public void showPlaylist()
    {
        closeX.SetActive(true);
       // iconBack.SetActive(true);
        playlist.SetActive(true);

        hideDrawer();

        

    }

    public void hidePlaylist()
    {
        closeX.SetActive(false);
        //iconBack.SetActive(false);
        playlist.SetActive(false);

        showDrawer();

        if (spotifyIsPaused == true)
        {
            sourceSpotify.GetComponent<spinGeneric>().enabled = false;
            spotifyIconStatusPaused.SetActive(true);

            Color tintBlack = new Color(0.8f, 0.8f, 0.8f);

            switch (currentTrackNum)
            {
                case 0:
                    albumArtBlueCircle.GetComponent<SpriteRenderer>().DOColor(tintBlack, 0.5f);
                    break;
                case 1:
                    albumArtRedCircle.GetComponent<SpriteRenderer>().DOColor(tintBlack, 0.5f);
                    break;
                case 2:
                    albumArtYellowCircle.GetComponent<SpriteRenderer>().DOColor(tintBlack, 0.5f);
                    break;
                default:
                    break;
            }

            
            
            

        }
        else
        {
            sourceSpotify.GetComponent<spinGeneric>().enabled = true;
            spotifyIconStatusPaused.SetActive(false);

            switch (currentTrackNum)
            {
                case 0:
                    albumArtBlueCircle.GetComponent<SpriteRenderer>().DOColor(Color.white, 0.5f);
                    break;
                case 1:
                    albumArtRedCircle.GetComponent<SpriteRenderer>().DOColor(Color.white, 0.5f);
                    break;
                case 2:
                    albumArtYellowCircle.GetComponent<SpriteRenderer>().DOColor(Color.white, 0.5f);
                    break;
                default:
                    break;
            }
        }
    }

    public void showOceanControls()
    {
        //oceanControls.SetActive(true);
        oceanControls.transform.localScale = new Vector3(1, 1, 1);
        oceanFullscreenBkg.transform.DOScale(new Vector3(8f, 6.4f, 8f),0.4f);
        oceanFullscreenBkg.transform.DOMove(new Vector3(0f, 0f, 0f), 0.4f).OnComplete(setOceanFullscreenParentToWorld);
        

        closeX.SetActive(true);
        sourceSpotify.transform.DOScale(new Vector3(0f, 0f, 0f), 0.4f);

        

        hideDrawer();
    }

    void setOceanFullscreenParentToWorld(){

        oceanFullscreenBkg.transform.parent = Camera.main.gameObject.transform;
    }

    void setOceanFullscreenParentToObject()
    {
        oceanFullscreenBkg.transform.parent = sourceOcean.gameObject.transform;
    }

    public void hideOceanControls()
    {
      //  oceanControls.SetActive(false);
        oceanControls.transform.localScale = new Vector3(0, 0, 0);
        setOceanFullscreenParentToObject();
        oceanFullscreenBkg.transform.DOScale(new Vector3(0f, 0f, 0f), 0.4f);
        sourceSpotify.transform.DOScale(new Vector3(6.2f, 6.2f, 6.2f), 0.4f);
        oceanFullscreenBkg.transform.DOLocalMove(new Vector3(0f, 0f, 0f), 0.4f);
        closeX.SetActive(false);

        showDrawer();
    }

    public void setCamView()
    {
        if (viewIsIso == true)
        {
            //camParent.transform.eulerAngles = new Vector3(0, 0, 0);
            camParent.transform.DORotate(new Vector3(0, 0, 0), 0.4f);
            iconIsoHighlight.GetComponent<SpriteRenderer>().DOFade(1, 0.2f);
            iconTopDownHighlight.GetComponent<SpriteRenderer>().DOFade(0, 0.2f);
        }
        else
        {
            //camParent.transform.eulerAngles = new Vector3(50, 0, 0);
            camParent.transform.DORotate(new Vector3(50, 0, 0), 0.4f);
            iconIsoHighlight.GetComponent<SpriteRenderer>().DOFade(0, 0.2f);
            iconTopDownHighlight.GetComponent<SpriteRenderer>().DOFade(1, 0.2f);
        }
    }

    public void hideDrawer()
    {
        drawerOcean.transform.DOScale(new Vector3(0, 0, 0), 0.15f);
        drawerSpotify.transform.DOScale(new Vector3(0, 0, 0), 0.15f);
    }

    public void showDrawer()
    {
        float scale = 1.4f;

        drawerOcean.transform.DOScale(new Vector3(scale, scale, scale), 0.15f);
        drawerSpotify.transform.DOScale(new Vector3(scale, scale, scale), 0.15f);
    }
}
