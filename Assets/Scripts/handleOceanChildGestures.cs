﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;

public class handleOceanChildGestures : MonoBehaviour
{
    public TapGesture doubleTap;
    bool isPressed = false;
    GameObject[] oceanChildren;
    handleState HandleState;
    float distanceFromCenter;
    public GameObject roomCenter;

    // Start is called before the first frame update
    void Start()
    {
        roomCenter = GameObject.Find("sphereSpace");
        doubleTap = gameObject.GetComponent<TapGesture>();
        doubleTap.Tapped += DoubleTap_Tapped;
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;
        oceanChildren = GameObject.FindGameObjectsWithTag("oceanChild");
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
    }

   

    // Update is called once per frame
    void Update()
    {
        distanceFromCenter = Vector3.Distance(transform.position, roomCenter.transform.position);
        //print("distance = " + distanceFromCenter);
    }

    void goToFocusState()
    {
        if (HandleState.oceanIsFocused == false)
        {
            HandleState.oceanIsFocused = true;
            showHideFocusState();
        }
        
    }

    void showHideFocusState()
    {
        //print("called showHideFocusState");

        if (HandleState.oceanIsFocused == true)
        {
            HandleState.showOceanControls();
        }
        else
        {
            HandleState.hideOceanControls();
        }
    }
        

    private void DoubleTap_Tapped(object sender, System.EventArgs e)
    {
        Debug.Log("double tappy child");

        if (HandleState.oceanIsFocused == false)
        {
            Invoke("goToFocusState", 0.1f);
        }
        

        HandleState.lastTouchedOceanObject = gameObject;
        HandleState.setLastTouchedOceanObject();


    }


    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
           // print("oceanChildPressed");

            
        }

    }



    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {

            //  print("ocean child released");

            


            if (distanceFromCenter > 3)
            {
                print("yooooo");
                //move it back to some midpoint back towards the sphere center
                Vector3 quarterWay = (transform.position + roomCenter.transform.position) / 3;
                transform.DOMove(quarterWay, 0.4f).SetEase(Ease.OutBack);

            }

            isPressed = false;

            


        }

    }
}
